# Index Builder

# CNH Redlining
# Jackson Voelkel

library(pacman)
p_load(dplyr,
       tidycensus,
       tmap,
       sf)

source("./variable_compilation.R")

# Tmap settings
tmap_mode("view")
tmap_options(basemaps = c("Stamen.TonerLite", "Stamen.Toner","OpenStreetMap","Esri.WorldImagery"))

# Variables in 2010 ACS
# acs_vars10 <- load_variables(2010, "acs5")

# Variables in 2020 ACS
# acs_vars20 <- load_variables(2020, "acs5")


#     ____  _            __                                     __
#    / __ \(_)________  / /___ _________  ____ ___  ___  ____  / /_
#   / / / / / ___/ __ \/ / __ `/ ___/ _ \/ __ `__ \/ _ \/ __ \/ __/
#  / /_/ / (__  ) /_/ / / /_/ / /__/  __/ / / / / /  __/ / / / /_
# /_____/_/____/ .___/_/\__,_/\___/\___/_/ /_/ /_/\___/_/ /_/\__/
#             /_/
# Displacement ------------------------------------------------------------

# Educational Attainment	% population over 25 without a Bachelor's Degree	ACS	2006-2010, 2016-2020	Block Groups

# Race	% population of color – population other than non-hispanic white	Census	2010, 2020	Block Groups

# # Income	% population below the federal poverty level	ACS	2006-2010, 2016-2020	Block Groups

# Gender	% female headed households	ACS	2006-2010, 2015-2019	Block Groups

# Vehicle	% households without a vehicle	Census	2006-2010, 2015-2019	Block Groups

# COMPILE -----------------------------------------------------------------

# Combine all 2010 data
dat_displacement_10 <- dat_edu_no_bachelors_bg$pre %>% 
  left_join(dat_race_ethnicity_poc_bg$pre) %>%
  left_join(dat_income_bg$pre) %>%
  left_join(dat_gender_bg$pre) %>%
  left_join(dat_vehicle_bg$pre) %>%
  left_join(dat_language_bg$pre) %>%
  left_join(dat_renter_occupied_bg$pre) %>%
  setNames(paste0(names(.), "_10")) %>%
  left_join(cross_w$bg, by = c("GEOID_10" = "GEOID_10")) %>% 
  replace(is.na(.), 0)

# Combine all 2020 data
dat_displacement_20 <- dat_edu_no_bachelors_bg$post %>% 
  left_join(dat_race_ethnicity_poc_bg$post) %>%
  left_join(dat_income_bg$post) %>%
  left_join(dat_gender_bg$post) %>%
  left_join(dat_vehicle_bg$post) %>%
  left_join(dat_language_bg$post) %>%
  left_join(dat_renter_occupied_bg$post) %>%
  setNames(paste0(names(.), "_20")) %>% 
  replace(is.na(.), 0)

# Join and collapse / summarise and then calculate differences
dat_displacement <- dat_displacement_10 %>% 
  left_join(dat_displacement_20) %>% 
  select(GEOID_10, sort(names(.))) %>% 
  distinct() %>% 
  # mutate(GEOID_10 = as.numeric(GEOID_10)) %>% 
  group_by(GEOID_10) %>% 
  summarise(
    bachelor_no_10 = unique(bachelor_no_10), 
    bachelor_no_20 = sum(bachelor_no_20, na.rm = T), 
    bachelor_no_MOE_10 = unique(bachelor_no_MOE_10), 
    bachelor_no_MOE_20 = sum(bachelor_no_MOE_20, na.rm = T), 
    bachelor_no_MOE_pct_10 = unique(bachelor_no_MOE_pct_10), 
    bachelor_no_MOE_pct_20 = sum(bachelor_no_MOE_pct_20, na.rm = T), 
    bachelor_no_pct_10 = unique(bachelor_no_pct_10), 
    bachelor_no_pct_20 = sum(bachelor_no_pct_20, na.rm = T), 
    gender_singlef_10 = unique(gender_singlef_10), 
    gender_singlef_20 = sum(gender_singlef_20, na.rm = T), 
    gender_singlef_MOE_10 = unique(gender_singlef_MOE_10), 
    gender_singlef_MOE_20 = sum(gender_singlef_MOE_20, na.rm = T), 
    gender_singlef_MOE_pct_10 = unique(gender_singlef_MOE_pct_10), 
    gender_singlef_MOE_pct_20 = sum(gender_singlef_MOE_pct_20, na.rm = T), 
    gender_singlef_pct_10 = unique(gender_singlef_pct_10), 
    gender_singlef_pct_20 = sum(gender_singlef_pct_20, na.rm = T), 
    GEOID_20 = paste(GEOID_20, collapse = "|"), 
    poc_10 = unique(poc_10), 
    poc_20 = sum(poc_20, na.rm = T), 
    poc_MOE_10 = unique(poc_MOE_10), 
    poc_MOE_20 = sum(poc_MOE_20, na.rm = T), 
    poc_MOE_pct_10 = unique(poc_MOE_pct_10), 
    poc_MOE_pct_20 = sum(poc_MOE_pct_20, na.rm = T), 
    poc_pct_10 = unique(poc_pct_10), 
    poc_pct_20 = sum(poc_pct_20, na.rm = T), 
    poverty_10 = unique(poverty_10), 
    poverty_20 = sum(poverty_20, na.rm = T), 
    poverty_MOE_10 = unique(poverty_MOE_10), 
    poverty_MOE_20 = sum(poverty_MOE_20, na.rm = T), 
    poverty_MOE_pct_10 = unique(poverty_MOE_pct_10), 
    poverty_MOE_pct_20 = sum(poverty_MOE_pct_20, na.rm = T), 
    poverty_pct_10 = unique(poverty_pct_10), 
    poverty_pct_20 = sum(poverty_pct_20, na.rm = T), 
    vehicle_no_10 = unique(vehicle_no_10), 
    vehicle_no_20 = sum(vehicle_no_20, na.rm = T), 
    vehicle_no_MOE_10 = unique(vehicle_no_MOE_10), 
    vehicle_no_MOE_20 = sum(vehicle_no_MOE_20, na.rm = T), 
    vehicle_no_MOE_pct_10 = unique(vehicle_no_MOE_pct_10), 
    vehicle_no_MOE_pct_20 = sum(vehicle_no_MOE_pct_20, na.rm = T), 
    vehicle_no_pct_10 = unique(vehicle_no_pct_10), 
    vehicle_no_pct_20 = sum(vehicle_no_pct_20, na.rm = T), 
    language_eng_no_10 = unique(language_eng_no_10), 
    language_eng_no_20 = sum(language_eng_no_20, na.rm = T), 
    language_eng_no_MOE_10 = unique(language_eng_no_MOE_10), 
    language_eng_no_MOE_20 = sum(language_eng_no_MOE_20, na.rm = T), 
    language_eng_no_MOE_pct_10 = unique(language_eng_no_MOE_pct_10), 
    language_eng_no_MOE_pct_20 = sum(language_eng_no_MOE_pct_20, na.rm = T), 
    language_eng_no_pct_10 = unique(language_eng_no_pct_10), 
    language_eng_no_pct_20 = sum(language_eng_no_pct_20, na.rm = T), 
    ro_renter_occupied_10 = unique(ro_renter_occupied_10), 
    ro_renter_occupied_20 = sum(ro_renter_occupied_20, na.rm = T), 
    ro_renter_occupied_MOE_10 = unique(ro_renter_occupied_MOE_10), 
    ro_renter_occupied_MOE_20 = sum(ro_renter_occupied_MOE_20, na.rm = T), 
    ro_renter_occupied_MOE_pct_10 = unique(ro_renter_occupied_MOE_pct_10), 
    ro_renter_occupied_MOE_pct_20 = sum(ro_renter_occupied_MOE_pct_20, na.rm = T), 
    ro_renter_occupied_pct_10 = unique(ro_renter_occupied_pct_10), 
    ro_renter_occupied_pct_20 = sum(ro_renter_occupied_pct_20, na.rm = T), 
  ) %>% 
  mutate(
    bachelor_no_diff = bachelor_no_20 - bachelor_no_10,
    bachelor_no_MOE_diff = bachelor_no_MOE_20 - bachelor_no_MOE_10,
    bachelor_no_MOE_pct_diff = round((bachelor_no_MOE_pct_20 - bachelor_no_MOE_pct_10) * 100, 4),
    bachelor_no_pct_diff = round((bachelor_no_pct_20 - bachelor_no_pct_10) * 100, 4),
    gender_diff = gender_singlef_20 - gender_singlef_10,
    gender_singlef_MOE_diff = gender_singlef_MOE_20 - gender_singlef_MOE_10,
    gender_singlef_MOE_pct_diff = round((gender_singlef_MOE_pct_20 - gender_singlef_MOE_pct_10) * 100, 4),
    gender_singlef_pct_diff = round((gender_singlef_pct_20 - gender_singlef_pct_10) * 100, 4),
    poc_diff = poc_20 - poc_10,
    poc_MOE_diff = poc_MOE_20 - poc_MOE_10,
    poc_MOE_pct_diff = round((poc_MOE_pct_20 - poc_MOE_pct_10) * 100, 4),
    poc_pct_diff = round((poc_pct_20 - poc_pct_10) * 100, 4),
    poverty_diff = poverty_20 - poverty_10,
    poverty_MOE_diff = poverty_MOE_20 - poverty_MOE_10,
    poverty_MOE_pct_diff = round((poverty_MOE_pct_20 - poverty_MOE_pct_10) * 100, 4),
    poverty_pct_diff = round((poverty_pct_20 - poverty_pct_10) * 100, 4),
    vehicle_no_diff = vehicle_no_20 - vehicle_no_10,
    vehicle_no_MOE_diff = vehicle_no_MOE_20 - vehicle_no_MOE_10,
    vehicle_no_MOE_pct_diff = round((vehicle_no_MOE_pct_20 - vehicle_no_MOE_pct_10) * 100, 4),
    vehicle_no_pct_diff = round((vehicle_no_pct_20 - vehicle_no_pct_10) * 100, 4),
    language_eng_no_diff = language_eng_no_20 - language_eng_no_10,
    language_eng_no_MOE_diff = language_eng_no_MOE_20 - language_eng_no_MOE_10,
    language_eng_no_MOE_pct_diff = round((language_eng_no_MOE_pct_20 - language_eng_no_MOE_pct_10) * 100, 4),
    language_eng_no_pct_diff = round((language_eng_no_pct_20 - language_eng_no_pct_10) * 100, 4),
    ro_renter_occupied_diff = ro_renter_occupied_20 - ro_renter_occupied_10,
    ro_renter_occupied_MOE_diff = ro_renter_occupied_MOE_20 - ro_renter_occupied_MOE_10,
    ro_renter_occupied_MOE_pct_diff = round((ro_renter_occupied_MOE_pct_20 - ro_renter_occupied_MOE_pct_10) * 100, 4),
    ro_renter_occupied_pct_diff = round((ro_renter_occupied_pct_20 - ro_renter_occupied_pct_10) * 100, 4),
  )

# Subset out only the variables we're interested in
dat_displacement_princ <- dat_displacement %>% 
  select(contains("pct_diff") & !contains("MOE"))

# Calculate principal component vector and z-score-ify it!
pc_displacement_all = princomp(dat_displacement_princ)
pc_displacement = princomp(dat_displacement_princ)$scores[,1] %>% 
  scale(center = T, scale = T) %>% 
  as.numeric() %>% 
  as.data.frame() %>% 
  `names<-`("pc") %>% 
  mutate(
    index = case_when((pc < -3) ~ -4,
                      (pc >= -3 & pc < -2) ~ -3,
                      (pc >= -2 & pc < -1) ~ -2,
                      (pc >= -1 & pc < 0) ~ -1,
                      (pc == 0) ~ 0,
                      (pc > 0 & pc <= 1) ~ 1,
                      (pc > 1 & pc <= 2) ~ 2,
                      (pc > 2 & pc <= 3) ~ 3,
                      (pc > 3) ~ 4,
                      TRUE ~ 10
    )) %>% 
  cbind(dat_displacement[,1], .) %>% 
  left_join(geom_10 %>% select("GEOID_10" = GEOID10)) %>% 
  cbind(dat_displacement_princ, .) %>% 
  left_join(cross_w$bg) %>% 
  select(GEOID_10, GEOID_20, everything()) %>% 
  st_as_sf()

# Map it!
tm_shape(pc_displacement %>% filter(grepl("^41067|^41005|^41051",GEOID_10))) +
  tm_polygons(
    col = "index",
    palette = "-RdBu",
    popup.vars = c("FIPS 2010" = "GEOID_10",
                   "FIPS 2020" = "GEOID_20",
                   "% Diff. 'No Bachelors'" = "bachelor_no_pct_diff",
                   "% Diff. 'POC'" = "poc_pct_diff",
                   "% Diff. 'Poverty'" = "poverty_pct_diff",
                   "% Diff. 'No Vehicle'" = "vehicle_no_pct_diff",
                   "% Diff. 'Not Well/No English'" = "language_eng_no_pct_diff",
                   "% Diff. 'Renter Occupied'" = "ro_renter_occupied_pct_diff")
    )

st_write(pc_displacement, "./.data/pc_displacement.gpkg")

#    ______           __       _ _____            __  _
#   / ____/__  ____  / /______(_) __(_)________ _/ /_(_)___  ____
#  / / __/ _ \/ __ \/ __/ ___/ / /_/ / ___/ __ `/ __/ / __ \/ __ \
# / /_/ /  __/ / / / /_/ /  / / __/ / /__/ /_/ / /_/ / /_/ / / / /
# \____/\___/_/ /_/\__/_/  /_/_/ /_/\___/\__,_/\__/_/\____/_/ /_/
# Gentrification ----------------------------------------------------------

# Income	% population above the average median income	ACS	2006-2010, 2016-2020	Block Groups

income_over_med_bg
income_over_med_tr

# Educational Attainment	% population over 25 with a Bachelor's Degree	ACS	2006-2010, 2016-2020	Block Groups

dat_edu_with_bachelors_bg
dat_edu_with_bachelors_tr

# Race	% white population - population that is non-hispanic white	Census	2010, 2020	Block Groups

dat_race_ethnicity_caw_bg
dat_race_ethnicity_caw_tr

# Homeownership	% owner occupied households	ACS	2006-2010, 2016-2020	Block Groups

dat_home_ownership_bg
dat_home_ownership_tr

# Property Values	RLIS Parcel Data	Metro	2010, 2020	Parcels (aggregate to block groups)

## This uses ACS instead!
dat_home_value_bg
dat_home_value_tr

# Housing Type (single or multi family)	RLIS Parcel Data	Metro	2010, 2020	Parcels (aggregate to block groups)

dat_housing_type_bg
dat_housing_type_tr

# Year Built for Residential Homes	RLIS Parcel Data	Metro	2010, 2020	Parcels (aggregate to block groups)

# dat_home_age_bg
# dat_home_age_tr

dat_homes2010_bg
dat_homes2010_tr

# COMPILE -----------------------------------------------------------------

# Combine all 2010 data
dat_gentrification_10 <- income_over_med_bg$pre %>% 
  left_join(dat_edu_with_bachelors_bg$pre) %>%
  left_join(dat_race_ethnicity_caw_bg$pre) %>%
  left_join(dat_home_ownership_bg$pre) %>%
  left_join(dat_home_value_bg$pre) %>%
  left_join(dat_housing_type_bg$pre) %>%
  # left_join(dat_homes2010_bg) %>%
  setNames(paste0(names(.), "_10")) %>%
  left_join(cross_w$bg, by = c("GEOID_10" = "GEOID_10")) %>% 
  replace(is.na(.), 0)

# Combine all 2020 data
dat_gentrification_20 <- income_over_med_bg$post %>% 
  left_join(dat_edu_with_bachelors_bg$post) %>%
  left_join(dat_race_ethnicity_caw_bg$post) %>%
  left_join(dat_home_ownership_bg$post) %>%
  left_join(dat_home_value_bg$post) %>%
  left_join(dat_housing_type_bg$post) %>%
  left_join(dat_homes2010_bg) %>%
  setNames(paste0(names(.), "_20")) %>% 
  replace(is.na(.), 0) %>% 
  rename("built_2010plus_pct" = "built_2010plus_pct_20")

# Join and collapse / summarise and then calculate differences
dat_gentrification <- dat_gentrification_10 %>% 
  left_join(dat_gentrification_20) %>% 
  select(GEOID_10, sort(names(.))) %>% 
  distinct() %>% 
  # mutate(GEOID_10 = as.numeric(GEOID_10)) %>% 
  group_by(GEOID_10) %>% 
  summarise(
    bachelor_yes_10 = unique(bachelor_yes_10), 
    bachelor_yes_20 = sum(bachelor_yes_20, na.rm = T), 
    bachelor_yes_MOE_10 = unique(bachelor_yes_MOE_10), 
    bachelor_yes_MOE_20 = sum(bachelor_yes_MOE_20, na.rm = T), 
    bachelor_yes_MOE_pct_10 = unique(bachelor_yes_MOE_pct_10), 
    bachelor_yes_MOE_pct_20 = sum(bachelor_yes_MOE_pct_20, na.rm = T), 
    bachelor_yes_pct_10 = unique(bachelor_yes_pct_10), 
    bachelor_yes_pct_20 = sum(bachelor_yes_pct_20, na.rm = T), 
    ho_owner_occupied_10 = unique(ho_owner_occupied_10), 
    ho_owner_occupied_20 = sum(ho_owner_occupied_20, na.rm = T), 
    ho_owner_occupied_MOE_10 = unique(ho_owner_occupied_MOE_10), 
    ho_owner_occupied_MOE_20 = sum(ho_owner_occupied_MOE_20, na.rm = T), 
    ho_owner_occupied_MOE_pct_10 = unique(ho_owner_occupied_MOE_pct_10), 
    ho_owner_occupied_MOE_pct_20 = sum(ho_owner_occupied_MOE_pct_20, na.rm = T), 
    ho_owner_occupied_pct_10 = unique(ho_owner_occupied_pct_10), 
    ho_owner_occupied_pct_20 = sum(ho_owner_occupied_pct_20, na.rm = T), 
    GEOID_20 = paste(GEOID_20, collapse = "|"), 
    ht_single_detached_10 = unique(ht_single_detached_10), 
    ht_single_detached_20 = sum(ht_single_detached_20, na.rm = T), 
    ht_single_detached_MOE_10 = unique(ht_single_detached_MOE_10), 
    ht_single_detached_MOE_20 = sum(ht_single_detached_MOE_20, na.rm = T), 
    ht_single_detached_MOE_pct_10 = unique(ht_single_detached_MOE_pct_10), 
    ht_single_detached_MOE_pct_20 = sum(ht_single_detached_MOE_pct_20, na.rm = T), 
    ht_single_detached_pct_10 = unique(ht_single_detached_pct_10), 
    ht_single_detached_pct_20 = sum(ht_single_detached_pct_20, na.rm = T), 
    built_2010plus_pct = mean(built_2010plus_pct), 
    medval_10 = unique(medval_10), 
    medval_20 = mean(medval_20, na.rm = T), 
    inc_over_med_pct_10 = unique(inc_over_med_pct_10), 
    inc_over_med_pct_20 = sum(inc_over_med_pct_20, na.rm = T), 
    re_nh_caw_10 = unique(re_nh_caw_10), 
    re_nh_caw_20 = sum(re_nh_caw_20, na.rm = T), 
    re_nh_caw_MOE_10 = unique(re_nh_caw_MOE_10), 
    re_nh_caw_MOE_20 = sum(re_nh_caw_MOE_20, na.rm = T), 
    re_nh_caw_MOE_pct_10 = unique(re_nh_caw_MOE_pct_10), 
    re_nh_caw_MOE_pct_20 = sum(re_nh_caw_MOE_pct_20, na.rm = T), 
    re_nh_caw_pct_10 = unique(re_nh_caw_pct_10), 
    re_nh_caw_pct_20 = sum(re_nh_caw_pct_20, na.rm = T)
  ) %>% 
  mutate(
    bachelor_yes_diff = bachelor_yes_20 - bachelor_yes_10,
    bachelor_yes_MOE_diff = bachelor_yes_MOE_20 - bachelor_yes_MOE_10,
    bachelor_yes_MOE_pct_diff = round((bachelor_yes_MOE_pct_20 - bachelor_yes_MOE_pct_10) * 100, 4),
    bachelor_yes_pct_diff = round((bachelor_yes_pct_20 - bachelor_yes_pct_10) * 100, 4),
    ho_owner_occupied_diff = ho_owner_occupied_20 - ho_owner_occupied_10,
    ho_owner_occupied_MOE_diff = ho_owner_occupied_MOE_20 - ho_owner_occupied_MOE_10,
    ho_owner_occupied_MOE_pct_diff = round((ho_owner_occupied_MOE_pct_20 - ho_owner_occupied_MOE_pct_10) * 100, 4),
    ho_owner_occupied_pct_diff = round((ho_owner_occupied_pct_20 - ho_owner_occupied_pct_10) * 100, 4),
    ht_single_detached_diff = ht_single_detached_20 - ht_single_detached_10,
    ht_single_detached_MOE_diff = ht_single_detached_MOE_20 - ht_single_detached_MOE_10,
    ht_single_detached_MOE_pct_diff = round((ht_single_detached_MOE_pct_20 - ht_single_detached_MOE_pct_10) * 100, 4),
    ht_single_detached_pct_diff = round((ht_single_detached_pct_20 - ht_single_detached_pct_10) * 100, 4),
    built_2010plus_pct = round(built_2010plus_pct * 100, 4),
    medval_diff = medval_20 - medval_10,
    inc_over_med_pct_diff = inc_over_med_pct_20 - inc_over_med_pct_10,
    re_nh_caw_diff = re_nh_caw_20 - re_nh_caw_10,
    re_nh_caw_MOE_diff = re_nh_caw_MOE_20 - re_nh_caw_MOE_10,
    re_nh_caw_MOE_pct_diff = round((re_nh_caw_MOE_pct_20 - re_nh_caw_MOE_pct_10) * 100, 4),
    re_nh_caw_pct_diff = round((re_nh_caw_pct_20 - re_nh_caw_pct_10) * 100, 4)
)

# Subset out only the variables we're interested in
dat_gentrification_princ <- dat_gentrification %>% 
  select((contains("pct_diff") | starts_with("medval_dif") | starts_with("built_2010plus_pct")) & !contains("MOE"))
# dat_gentrification_princ_complete <- complete.cases(dat_gentrification_princ)
dat_gentrification_princ_complete <- dat_gentrification_princ %>% replace(is.na(.), 0)

# dat_gentrification %>% filter(grepl("^41051",GEOID_10)) %>% left_join(geom_10, by = c("GEOID_10" = "GEOID10")) %>% st_as_sf() %>% tm_shape + tm_polygons(fill = 'red', alpha = 0.4)

# Calculate principal component vector and z-score-ify it!
pc_gentrification_all = princomp(dat_gentrification_princ_complete)
pc_gentrification = princomp(dat_gentrification_princ_complete)$scores[,1] %>% 
  scale(center = T, scale = T) %>% 
  as.numeric() %>% 
  as.data.frame() %>% 
  `names<-`("pc") %>% 
  mutate(
    index = case_when((pc < -3) ~ -4,
                      (pc >= -3 & pc < -2) ~ -3,
                      (pc >= -2 & pc < -1) ~ -2,
                      (pc >= -1 & pc < 0) ~ -1,
                      (pc == 0) ~ 0,
                      (pc > 0 & pc <= 1) ~ 1,
                      (pc > 1 & pc <= 2) ~ 2,
                      (pc > 2 & pc <= 3) ~ 3,
                      (pc > 3) ~ 4,
                      TRUE ~ 10
    )) %>% 
  cbind(dat_gentrification[,1], .) %>% 
  left_join(geom_10 %>% select("GEOID_10" = GEOID10)) %>% 
  cbind(dat_gentrification_princ_complete, .) %>% 
  left_join(cross_w$bg) %>% 
  select(GEOID_10, GEOID_20, everything()) %>% 
  st_as_sf()


tm_shape(pc_gentrification %>% filter(grepl("^41067|^41005|^41051",GEOID_10))) +
tm_polygons(
  col = "index",
  n = 9,
  palette = "-RdBu",
  popup.vars = c(
    "% change: with Bachelors" = "bachelor_yes_pct_diff",
    "% change: Owner Occupied Homes" = "ho_owner_occupied_pct_diff",
    "% change: Single Detached Homes" = "ht_single_detached_pct_diff",
    "% change: Households over median County Income" = "inc_over_med_pct_diff",
    "% change: Non-Hispanic White" = "re_nh_caw_pct_diff",
    "Change: Median Home Value" = "medval_diff",
    "Homes Built Since 2010" = "built_2010plus_pct"
    )
)

# saveRDS(pc_gentrification, "./.data/pc_gentrification.RDS")
# st_write(pc_gentrification, "./.data/pc_gentrification.gpkg")

#     ______          __           _               ___
#    / ____/  _______/ /_  _______(_)   _____     /   |  ________  ____ ______
#   / __/ | |/_/ ___/ / / / / ___/ / | / / _ \   / /| | / ___/ _ \/ __ `/ ___/
#  / /____>  </ /__/ / /_/ (__  ) /| |/ /  __/  / ___ |/ /  /  __/ /_/ (__  )
# /_____/_/|_|\___/_/\__,_/____/_/ |___/\___/  /_/  |_/_/   \___/\__,_/____/
# Exclusive Areas ---------------------------------------------------------

# Income	% population over 120% of the average median income	ACS	2006-2010, 2016-2020	Block Groups

## Used 125%, far easier
pullvars_gt125_FPL_type_bg
pullvars_gt125_FPL_type_tr

# Educational Attainment	% population over 25 with a Bachelor's Degree	ACS	2006-2010, 2016-2020	Block Groups

dat_edu_with_bachelors_bg
dat_edu_with_bachelors_tr

# Race	% white population - population that is non-hispanic white	Census	2010, 2020	Block Groups

dat_race_ethnicity_caw_bg
dat_race_ethnicity_caw_tr

# Homeownership	% owner occupied households	ACS	2006-2010, 2016-2020	Block Groups

dat_home_ownership_bg
dat_home_ownership_tr

# Property Values	RLIS Parcel Data	Metro	2010, 2020	Parcels (aggregate to block groups)

dat_home_value_bg
dat_home_value_tr

# Year Built for Residential Homes	RLIS Parcel Data	Metro	2010, 2020	Parcels (aggregate to block groups)

dat_home_age_bg
dat_home_age_tr

# Lot Size / Building Footprint of Residential Homes	RLIS Parcel Data	Metro	2010, 2020	Parcels (aggregate to block groups)

### THIS DATA IS MISSING; 
# TODO: Should we replace this with "housing units per square mile"? In 'big lot' neighborhoods, this will be very small, but in dense areas it'll be high!

### SHOULD MISSING VALUES BE REPLACED WITH A LAG??