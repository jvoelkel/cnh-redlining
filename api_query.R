# Handling API calls with and RDS caching mechanism

pull_ipums <- function(geography, year, variables, state, my_key = NULL, cache = TRUE){
  # REQUIRE PACKAGES
  library(pacman)
  p_load(httr,
         ipumsr,
         jsonlite)
  
  # SET ACS RANGES
  acs_version = paste0(year-4,"_",year,"_ACS5a")
  
  # Get distinct table names to pull from IPUMS
  tabs = variables %>% 
    gsub("_.*","",.) %>% 
    unique()
  
  # Destination file(s)
  output_RDS = paste0("./.cache/",paste(tabs,collapse = "_"),"-vintage",acs_version,"-geog_",gsub(" ", "_",geography),".RDS")
  zip_file = paste0("./.cache/",paste(tabs,collapse = "_"),"-vintage",acs_version,"-NHGIS-tables.zip")
  
  # Check if we need to pull the data or can just use cached data
  if(!file.exists(output_RDS) | !cache){
    
    # If the file is missing or I force it to remake it, run this
    
    # GET KEY
    if(missing(my_key)){
      my_key = Sys.getenv("IPUMS_API_KEY")
      if(nchar(my_key) == 0){
        stop("Please set env var IPUMS_API_KEY in your .Renviron file or manually enter key!")
      }
    }
    
    url = "https://api.ipums.org/extracts/?product=nhgis&version=v1"
    mybody = paste0('{
    "datasets": {
      "',acs_version,'": {
        "data_tables": [',
    paste0('"',tabs,'"',collapse=','),'
        ],
        "geog_levels": [
          "',ifelse(geography == "block group", 'blck_grp', geography), # Set geography to match IPUMS naming
          '" 
        ]
      }
  
    },
    "geographic_extents": ["*"],
    "data_format": "csv_no_header",
    "description": "test_api_auto",
    "breakdown_and_data_type_layout": "single_file"
  }
  
  ')
    
    mybody_json = fromJSON(mybody, simplifyVector = FALSE)
    result = POST(url, add_headers(Authorization = my_key), body = mybody_json, encode = "json")
    res_df = content(result, "parsed", simplifyDataFrame = TRUE)
    my_number = res_df$number
    
    # If there was an error, try and diagnose it
    if(is.null(my_number)){
      tryCatch({
        if(!is.null(res_df$detail)){
          stop(res_df$detail)
        }
      }, finally = {
        stop("AN ERROR OCCURRED")
      })
    }
    
    # Get status --------------------------------------------------------------
    test_done = NULL
    
    while(is.null(test_done)){
      data_extract_status_res = GET(paste0("https://api.ipums.org/extracts/",my_number,"?product=nhgis&version=v1"), add_headers(Authorization = my_key))
      des_df = content(data_extract_status_res, "parsed", simplifyDataFrame = TRUE)
      # des_df
      test_done = des_df$download_links$table_data
      message("Waiting for data to be ready...")
      Sys.sleep(10)
    }
    
    # Read --------------------------------------------------------------------
    
    # Retrieve the file from the URL and read it into R using the ipumsr 
    
    # Download table data and read into a data frame
    # Download extract to destination file
    download.file(des_df$download_links$table_data, zip_file, headers=c(Authorization=my_key))
    # List extract files in ZIP archive
    # unzip(zip_file, list=TRUE)
    # Read 2000 block-group CSV file into a data frame
    docs = unzip(zip_file, list=TRUE) %>% filter(grepl("\\.txt",Name)) %>% pull(Name)
    unzip(zipfile = zip_file, 
          files = docs, 
          exdir = "./.cache/")
    output_data = read_nhgis(zip_file, data_layer = contains("blck_grp.csv"))
    
    # Get translations
    output_dict = grep("NHGIS code|Source code", readLines(file.path("./.cache",docs)), value = T) %>%  # read the dictionary
      grep(acs_version, ., invert = T, value = T) %>% # pull out relevent information
      trimws() %>% 
      gsub(".*: ","",.) %>% # remove descriptors 
      matrix(ncol = 2, byrow = T) %>%  # convert to 2xn
      as.data.frame() %>% 
      `colnames<-`(c("ACS","IPUMS")) %>% # add headers
      distinct()
    
    # "Fix" names:
    # Replace old names with new names a la ipums dictionary
    new_names = data.frame(variables, ACS=substr(variables,1,6)) %>% 
      left_join(output_dict, by = "ACS") %>%
      apply(1, function(x){ # For each code, update to new IPUMS code name
        trimws(gsub(substr(x[1],1,7), paste0(x[3],"E"),x[1]))
      })
    
    # Make new names for the moe's too
    new_names_moe = data.frame(variables, ACS=substr(variables,1,6)) %>% 
      left_join(output_dict, by = "ACS") %>%
      apply(1, function(x){ # For each code, update to new IPUMS code name
        trimws(gsub(substr(x[1],1,7), paste0(x[3],"M"),x[1]))
      })
    
    # Compile and clean data to match what `tidycensus` would have pulled
    var_10 = output_data %>% 
      mutate(fips10 = paste0(STATEA,COUNTYA,TRACTA,BLKGRPA)) %>%
      select(fips10, new_names, new_names_moe) %>%
      `colnames<-`(c("fips10", paste0(variables,"E"), paste0(variables,"M") )) %>% 
      remove_all_labels()
    
    # Add back names
    new_colnames = colnames(var_10) %>%
      as.data.frame() %>%
      `colnames<-`('old_full') %>% 
      mutate(old = gsub("M$|E$", "", old_full)) %>% 
      left_join(data.frame(new = c("fips10",names(variables)),
                           old = c("fips10", variables)),
        by = "old") %>% 
      mutate(new = paste0(new, gsub(".*_[0-9][0-9][0-9]", "", gsub("fips10","",old_full)) )) # Add back the E/M flags
    
    colnames(var_10) = new_colnames$new
    
    if(cache){
      saveRDS(var_10, output_RDS)
    }
    
    # cleanup temp files
    unlink(file.path("./.cache",dirname(docs)), recursive = T)
    unlink(zip_file)
    
  } else {
    # if it already exists, just read it
    message(paste0("Loading cached data from ", getwd(), substr(output_RDS, 2, nchar(output_RDS))))
    var_10 = readRDS(output_RDS)
  }
  return(var_10)
}

pull_acs <- function(geography, variables, year, output, var, cache = TRUE, ...){
  all_states <- tidycensus::fips_codes %>% 
    pull(state) %>% 
    unique() %>% 
    .[1:51]
  output_RDS = paste0("./.cache/CENSUS-year_",year,"-var_",var,"-geog_",gsub(" ","_",geography),".RDS")
  if(!file.exists(output_RDS) | !cache){
    x = get_acs(geography = geography,
                variables = variables,
                year = year,
                state = all_states,
                output = output,
                var = var,
                cache = cache)
    if(cache){
      saveRDS(x, output_RDS)
    }
  } else {
    message(paste0("Loading cached data from ", getwd(), substr(output_RDS, 2, nchar(output_RDS))))
    x = readRDS(output_RDS)
  }
  return(x)
}
